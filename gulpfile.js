const gulp = require('gulp');
const sass = require('gulp-sass');
const browserSync = require('browser-sync').create();
const cleanCSS = require('gulp-clean-css');
const clean = require('gulp-clean');
const autoprefixer = require('gulp-autoprefixer');
const concat = require('gulp-concat');
const jsMin = require('gulp-jsmin');
const imageMin = require('gulp-imagemin');

gulp.task('clean', function () {
    return gulp.src('dist/*')
        .pipe(clean({ force: true }));
});

const style = () => {
    return gulp
        .src("src/scss/*.scss")
        .pipe(sass())
        .pipe(cleanCSS())
        .pipe(autoprefixer({"overrideBrowserslist": 'last 2 versions'}))
        .pipe(concat('styles.min.css'))
        .pipe(gulp.dest("dist/css/"))
        .pipe(browserSync.stream())
}

const scripts = () => {
    return gulp
        .src("src/js/**/*.js")
        .pipe(jsMin())
        .pipe(concat('scripts.min.js'))
        .pipe(gulp.dest("dist/js/"))
        .pipe(browserSync.stream())
}

const img = () => {
    return gulp
        .src('./src/img/**')
        .pipe(imageMin())
        .pipe(gulp.dest('./dist/img'));
}

const watch = () => {
    browserSync.init({
        server: {
            baseDir:'./'
        }
    });
    gulp.watch('src/scss/**/*.scss', style);
    gulp.watch('src/js/**/*.js', scripts);
    gulp.watch('src/img/**', img);
    gulp.watch('*html').on('change', browserSync.reload);
}

gulp.task('build',gulp.series('clean',gulp.parallel (style, scripts, img)));
gulp.task('watch', watch);

gulp.task('dev', gulp.series([
    'build',
    'watch'
]))




