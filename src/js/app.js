const menuBtn = document.querySelector(".site-nav__menu-btn");
const mainMenu = document.querySelector(".site-menu");

const toggleMobileMenu = () => {
    menuBtn.classList.toggle('site-nav__menu-btn--show-menu');
    menuBtn.classList.toggle('site-nav__menu-btn--hide-menu');
    mainMenu.classList.toggle('show-menu');
};

menuBtn.onclick = toggleMobileMenu;
window.onclick = event => {
    if (event.target !== menuBtn && mainMenu.classList.contains('show-menu')) {
            toggleMobileMenu();
        }
}


